export function getEnvironment<T extends object>(src: T): Readonly<T>
export function getEnvironment<T extends object>(src: T, options: {
  writable: false
}): Readonly<T>
export function getEnvironment<T extends object>(src: T, options: {
  writable: true
}): T
export function getEnvironment<T extends object>(src: T): T {
  const ret = {} as Record<keyof T, any>;

  for (const key in src) {
    const type = typeof src[key];
    const env = process.env[key];

    if (env) {
      switch (type) {
        case "boolean":
          ret[key] = env === "true";
          break;
        case "bigint":
        case "number":
          ret[key] = +env;
          break;
        case "string":
        case "symbol":
          ret[key] = env.toString();
          break;
        default:
          ret[key] = JSON.parse(env);
      }
    } else {
      ret[key] = src[key];
    }
  }

  return ret;
}
